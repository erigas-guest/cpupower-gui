cpupower-gui (0.9.1-2) UNRELEASED; urgency=medium

  * Drop debian/patches
  * Change d/rules - Enable DH_VERBOSE

 -- Evangelos Rigas <e.rigas@cranfield.ac.uk>  Mon, 02 Nov 2020 23:19:10 +0000

cpupower-gui (0.9.1-1) unstable; urgency=medium

  * debian/control: change architecture
    - Package is architecture independent
  * d/cpupower-gui.1: Fix typo in manpage
  * debian/rules: Add libdir and libexec flags
    debian/patches: Add patch to install helper in libexec
  * d/lintian-overrides: Add new entry
    - Add entry for dbus helper service unit
    - Add patches for service units
  * New upstream version 0.9.1
  * Prepare to release

 -- Evangelos Rigas <e.rigas@cranfield.ac.uk>  Fri, 16 Oct 2020 19:26:50 +0100

cpupower-gui (0.9.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

  [ Evangelos Rigas ]
  * New upstream version 0.9.0
  * debian/rules: Update meson flags
  * d/control: Update dh-compat and dependencies
  * d/cpupower-gui.1: Update manpage
  * debian/copyright: Update year info

 -- Evangelos Rigas <e.rigas@cranfield.ac.uk>  Thu, 15 Oct 2020 13:47:26 +0100

cpupower-gui (0.8.0-2) unstable; urgency=medium

  * Override autoconfigure to provide custom flags

 -- Evangelos Rigas <e.rigas@cranfield.ac.uk>  Tue, 25 Feb 2020 11:40:37 +0000

cpupower-gui (0.8.0-1) unstable; urgency=medium

  * Drop patches
  * New upstream version 0.8.0
  * Update debian/control
  * Add debian/lintian-overrides

 -- Evangelos Rigas <e.rigas@cranfield.ac.uk>  Sun, 26 Jan 2020 13:36:07 +0000

cpupower-gui (0.7.2-2) unstable; urgency=medium

  * Add patch to fix desktop filename

 -- Evangelos Rigas <e.rigas@cranfield.ac.uk>  Fri, 29 Nov 2019 22:40:57 +0000

cpupower-gui (0.7.2-1) unstable; urgency=medium

  * Update watch file and gbp.conf
  * New upstream version 0.7.2
  * Update d/patches
  * Bump Standards-Version to 4.4.1 and update dependencies
  * Update d/rules
  * Update d/copyright

 -- Evangelos Rigas <e.rigas@cranfield.ac.uk>  Tue, 26 Nov 2019 00:15:49 +0000

cpupower-gui (0.7.0-1) unstable; urgency=medium

  * Initial release. (Closes: #940496: ITP: cpupower-gui -- A graphical
    program for changing the scaling frequency limits of the cpu)

 -- Evangelos Rigas <e.rigas@cranfield.ac.uk>  Fri, 20 Sep 2019 19:49:32 +0100
